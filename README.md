# pyvoxel_opensource

This code makes the Voxel methodology applicable to shading studies, especially in the urban environment. <br>
It provides a basic understanding of the functions and can be further developed for specific context. <br>
For further information see <br>https://www.researchgate.net/publication/350994411_Voxel_based_method_for_real-time_calculation_of_urban_shading_studies

## Getting started is possible in 2 ways

### 1.) Use the precalculated neighbourhood (no use of Grasshopper components necessary)

To make things easily available, we precalculated a neighborhood located in Kempten, Germany.
This does not require any precalculation and can directly be run from the Notebook provided with this Repository.
Download the precalculated files from:
https://syncandshare.lrz.de/getlink/fiBfNRhyniPJqtvawpdjWv1Q/

##### Use of the precalculated data:
After extracting the folders and placing them in the 'intermediary_data' folder of the Repository, run the .ipynb in a Jupyter Notebook. <br>
Run the whole Notebook to check if things work. You should see a heatmap of the area in the end.

![This is how the results should look like with intermediary data](/f99_images/resulting_sunlight.png "Resulting Image Sunlight hours")

Play around with the input position of the object ("User Input").<br>
You can input the object several times and overlap the shading masks by uncommenting the two lines in the simulate_shade() function.<br>
To input more objects, copy the two lines and add own positions.

### 2.) Creating new shading-voxels and context binaries (Grasshoper use necessary)

If you want to apply the Voxel approach to another urban setting, the precalculation has to be performed for this specific context. <br>
The context contains a status quo of buildings and other surrounding objects which you want to include in your study (BaseCase). <br>
Furthermore, the object to insert (in our example file a tree) is simulated and saved separately.

##### Requirements
The following software packages are required in order to create the binaries:<br>
Rhinoceros 3D Version 6<br>
Ladybug-Legacy (see food4rhino.com)<br>
An Anaconda environment with pandas installed<br>

##### Precalculation in Grasshopper:
To create the voxel matrices for the object and the base case, perform the following steps:<br>
Change the .epw weatherfile in "\Voxel_OpenSource\f01_import\raw_data\weather_URBAN.epw (the file has to be named "weather_URBAN.epw") <br>
Open the voxel_generator_version.gh<br>
Create a file with sun vectors by switching the first toogle from false to true (The file is created in f01_import\raw_data)<br>
Set your baseplane for the simulation (Surface). The dimensions have to be a full factor of your chosen gridsize. The surface should be located at the zero-level of your model (z=0).<br>
Set your context for the simulation (BREPs of buildings and surrounding objects). Removal of objects which are included in this step will not be possible with the current development.<br>
Run the context simulation by switching the second toogle from false to true (The files are created in f01_import\raw_data\sunlight_hours_base).<br>
Set your object for the simulation (a tree in the example file, BREP).<br>
Run the object simulation by switching the third toogle from false to true (The files are created in f01_import\raw_data\sunlight_hours).<br>
Run the dimension writer by switching the fourth toogle from false to true (The file is created in f01_import\raw_data).<br>
Your files are prepared for the import now, you can close Grasshopper and Rhino and switch to the Jupyter Notebook.<br>

##### Importing and simulating with the new files in Jupyter Notebook:
Set reload_intermediary to False.<br>
Set the grid_size variable in "User Inputs" to the same gridsize as it was in the Grasshopper file.<br>
Choose an input position for your object (x,y,z).<br>
Run the whole Notebook.<br>
New intermediary files will be created in "f01_import\intermediary_data".<br>
You can change the reload_intermediary variable to True after the first import, this will speed up reloading for the next use.<br>

## Authors and acknowledgment
This work was conducted within the research project 'Densification in context of climate change'. The  project is funded by Bavarian State Ministry of the Environment and Consumer Protection (StMUV) and the TUM Centre for Urban Ecology and Climate Adaption (ZSK). For further information see <br>

https://www.researchgate.net/publication/350994411_Voxel_based_method_for_real-time_calculation_of_urban_shading_studies <br>
https://www.zsk.tum.de/en/zsk/home/


<img src="./f99_images/StMUV_engl.jpg" width="350"/> <img src="./f99_images/ZSK.jpg" width="80"/>



## License
This project is under MIT license, see license file.
