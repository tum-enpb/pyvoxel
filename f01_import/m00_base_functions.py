def read_dimensions(file):
    with open(file, 'r') as f:
        first_line = f.readlines()[0]
    dimensions = tuple([int(dim) for dim in first_line.strip().split(',')])
    return dimensions