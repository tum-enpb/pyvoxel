import os
import pickle
import pandas as pd
import numpy as np

def get_key(string):
    try:
        return int(string.strip('results').strip('.pkl'))
    except ValueError:
        return 999

def import_base_case(path, path_points):
    """Import the preprocessed base-case simulation results into a voxel dataframe

    Parameters
    ----------
    path: str
        Path to the result folder.
    path_points
        Path to the result points file.
    Returns
    -------
    dataframe
        Voxel dataframe containing the preprocessed sunlight hours simulation results
    """
    dfs = []
    for item in sorted(os.listdir(path), key=get_key):
        if 'results' in item:
            lists = pickle.load(open(path + item, 'rb+'))
            df = pd.DataFrame(lists)
            dfs.append(df)
    df_results = pd.concat(dfs)
    df_results.index = range(len(df_results.index))
    df_coordinates = pd.read_csv(path_points, header=None)
    df_coordinates.columns = ['x', 'y', 'z']
    for column in df_coordinates.columns:
        if df_coordinates[column].min() < 0:
            df_coordinates[column] = df_coordinates[column] + abs(df_coordinates[column].min())
    df_coordinates['z'] = df_coordinates['z']
    df_coordinates.index = range(len(df_coordinates.index))
    df_combined = pd.concat([df_coordinates, df_results], axis=1)
    df_combined.index = [(int(df_combined['x'][i]), int(df_combined['y'][i]), int(df_combined['z'][i])) for i in
                         range(len(df_combined.index))]
    df_combined = df_combined.drop(['x', 'y', 'z'], axis=1)
    df_combined.index = pd.MultiIndex.from_tuples(df_combined.index)
    df_combined = (df_combined == 0)
    return df_combined
