import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

def pivot(df_cut_sum, typ, mode):
    """Pivot a voxel dataframe according to its mode

    Parameters
    ----------
    df_cut_sum: pandas.Dataframe
        Voxel dataframe to be pivotted
    typ: str
        Voxel dataframe typ to be pivotted; Only 'Sunlight' possible at the moment
    mode: str
        Accumulation mode
    Returns
    -------
    pandas.DataFrame
        Pivotted voxel dataframe
    """
    id = df_cut_sum.name
    df_cut_sum = pd.DataFrame(df_cut_sum)
    df_cut = df_cut_sum
    df_cut_sum['x'] = df_cut.index.get_level_values(0)
    df_cut_sum['y'] = df_cut.index.get_level_values(1)
    fig, axes = plt.subplots(figsize=(10, 5))
    pivotted = df_cut_sum.pivot('y', 'x', id)
    pivotted = pivotted.sort_index(ascending=False)
    if typ == 'Sunlight':
        sns.heatmap(pivotted, axes=axes, cbar_kws={'label': 'Sunlight hours whole year sum'})
        axes.set_title(typ)
        plt.show()


def horizontal(df, height, mode, typ):
    """Make an horizontal intersection of a voxel dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        Voxel dataframe
    height: int
        Intersection height
    mode: str
        Time accumulation mode, 'sum'
    typ: str
        Voxel dataframe type

    Returns
    -------

    """
    df_cut = df.loc[df.index.get_level_values(2) == height]
    df_cut_sum = pd.DataFrame()
    if mode == 'sum':
        df_cut_sum['sum'] = df_cut.sum(axis=1)
        df_cut_sum = abs(df_cut_sum - df_cut_sum.max())
        pivot(df_cut_sum['sum'], typ, mode)


