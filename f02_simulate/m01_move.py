import pandas as pd
import numpy as np

def move(df, x, y, z, dimensions, gridsize):
    """Move all voxels in a voxel dataframe

    Parameters
    ----------
    df: pandas.DataFrame
        Voxel dataframe
    x: int
        X-move to apply to voxels
    y: int
        Y-move to apply to voxels
    z: int
        Z-move to apply to voxels
    dimensions: tuple
        (x, y, z) tuple indicating the dimensions of the project area

    Returns
    -------
    pandas.DataFrame
        Moved voxel dataframe
    """
    df1 = df.copy()
    x = my_round(x,gridsize)
    y = my_round(y,gridsize)
    z = my_round(z,gridsize)
    index = df1.reset_index()[['level_0', 'level_1', 'level_2']].applymap(lambda x: my_round(x, gridsize)).values.astype(int)
    transformation = np.array((x, y, z)).astype(int)
    index = index + transformation
    mask = [all([ind[0] <= dimensions[0], ind[1] <= dimensions[1], ind[2] <= dimensions[2]]) and all([ind[0] >= 0, ind[1] >= 0, ind[2] >= 0])for ind in index]
    index = pd.MultiIndex.from_tuples(tuple(map(tuple, index)))
    df1.index = index
    df1 = df1.loc[mask]
    return df1


def move_individual(df_global, df, x, y, z, mode, dimensions, gridsize):
    """Join voxel dataframes at the specified locations

    Parameters
    ----------
    df_global: pandas.DataFrame
        Global voxel dataframe
    df: pandas.DataFrame
        Local voxel dataframe
    xs: int, list
        X-coordinates of insertion points
    ys: int, list
        Y-coordinates of insertion points
    zs: int, list
        Z-coordinates of insertion points
    mode: str
        Insertion mode, only 'sunlight' for the moment
    dimensions: tuple
        (x, y, z) tuple indicating the dimensions of the project area

    Returns
    -------
    pandas.DataFrame
        Joined voxel dataframes

    """
    df_moved = move(df, x, y, z, dimensions, gridsize)

    if mode == 'sunlight':
        df_global_part = df_global.loc[df_moved.index] * 1
        df_global_part = df_global_part.add(df_moved, fill_value=0)
        df_global_part = (df_global_part > 0)
        df_global.loc[df_moved.index] = df_global_part

    return df_global

def my_round (x, base):
    return base * round(x/base)